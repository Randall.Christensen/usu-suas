#include "path_follower_example.h"


namespace rosplane
{

path_follower_example::path_follower_example()
{
}

void path_follower_example::follow(const params_s &params, const input_s &input, output_s &output)
{
 // implement line following and orbit following in Chapter 10 
}

} //end namespace
